from fastapi import FastAPI
from os import environ as env
from src.helper_functions.driver_connect import driver_connect

from selenium.webdriver.common.by import By

app = FastAPI()

@app.get("/")
def index():
    return {"details":"Hello world"}

@app.get('/test')
def test_page():
    url = "https://iatse15.unionimpact.com/login"
    def test_scrape():
        driver = driver_connect(True)
        driver.get(url)
        page_data = driver.page_source
        print(page_data)
        driver.close()
        return page_data 
        
    return {"content":test_scrape()}


