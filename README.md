# Union_Assist_API

## Name

Impact_assistant API server.

## Description

This will be a FastAPI server useing selinum to pull data from a dispatch service and allow for sorting and better unerstanding of schedule data
with Only Local Storage of user information.

It will communicate with a Flutter Front end.

### Deployment

GCP deployment [impact_assist](https://impact-assist-api-skhf7pzptq-uw.a.run.app/pi)

## Badges

To Come

## Installation

### Developers

- clone repo
- set up virtual env (venv or other)
- install requirements.txt `pip install -r requirements.txt`

- run `python -m uvicorn main:app --port=<####> --reload`

## Usage

This will be a FastAPI server useing selinum to pull data from a dispatch service and allow for sorting and better unerstanding of schedule data
with Only Local Storage of user information.

It will communicate with a Flutter Front end.

## Support

For support you have a few options:

- if it is a code problem please submit an issue on the gitlabs [repo](https://gitlab.com/M-Ravenmoore/impact-assist-api/-/issues)
- you can email the project at <dev.impact.assist@gmail.com>
- the program also has a [Discord](https://discord.gg/FERuMPXtp4)

## Roadmap

The Application MVP will be a multi platform app that can Collect Event/position data from union impact. Then allow users to do basic sort functions on that data. And finaly submit a orginized bid for jobs.

After MVP is met our goals list can be found in [future features](.\readme_docs\future_features.md)

## Contributing

Information soon to come
I am working out how best people can contribute. For now the best contributions is to check out the Prototype app on windows [download](https://tinyurl.com/impact-assist) and leave feedback on it.

Current working board is Here [trello board](https://trello.com/b/fOcRdK6A/impact-assistant-workboard)

## Authors and acknowledgment

Matt Ravenmoore (Lead developer)
Chris Hudson (codeing support and review)

## License

MIT open source licence

## Project status

In Primary Development No Code Base Yet
